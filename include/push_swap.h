/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/28 20:51:36 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/25 18:42:50 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

/*
** Set default algorithm to 1 (Oliver)
*/
# ifndef ALGORITHM
#  define ALGORITHM 1
# endif

/*
** INCLUDES:
** stack.h: init_stack()
** stdbool.h: bool
** sys/types: ssize_t type
*/

# include <stack.h>
# include <stdbool.h>
# include <sys/types.h>

void	oliver_algorithm(t_stack **a, t_stack **b, bool verbose);
void	chunk_algorithm(t_stack **a, t_stack **b, bool verbose);

ssize_t	search_chunk(t_stack *stack, struct s_int_array *sorted, int n_chunks,
			int chunk);
void	display_instruction(const char *instruction, bool verbose,
			t_stack *a, t_stack *b);
void	sort_size_three(t_stack **a, t_stack **b, bool verbose);
void	sort_size_five(t_stack **a, t_stack **b, bool verbose);
void	reverse_move_a_to_top(t_stack **a, t_stack *b, ssize_t index,
			bool verbose);
void	move_a_to_top(t_stack **a, t_stack **b, size_t index, bool verbose);
void	reverse_move_b_to_top(t_stack *a, t_stack **b, ssize_t index,
			bool verbose);
void	move_b_to_top(t_stack **a, t_stack **b, size_t index, bool verbose);
void	push_to_b(t_stack **a, t_stack **b, bool verbose);
void	rotate_empty_stack_b(t_stack **a, t_stack **b, bool verbose);

#endif
