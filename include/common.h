/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/21 19:57:55 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/02 20:32:46 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMMON_H
# define COMMON_H

/*
** INCLUDES:
** stddef.h: size_t type
*/

# include <stddef.h>

struct s_int_array
{
	int		*nums;
	size_t	length;
};

void	res_itoa(char *str, size_t max_len, int n);
void	error_location(const char *file, int line, const char *msg);
void	swap_ints(int *a, int *b);
int		get_next_line(int fd, char **line);
void	sort_int_array(struct s_int_array *array);
void	delete_int_array(struct s_int_array *array);

#endif
