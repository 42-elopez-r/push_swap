/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instructions.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/25 18:25:09 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/25 18:59:29 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INSTRUCTIONS_H
# define INSTRUCTIONS_H

/*
** INCLUDES:
** stack.h: t_stack
*/

# include <stack.h>

typedef void	(*t_instruction)(t_stack **a, t_stack **b);

t_instruction	get_instruction(const char *instruction);

void			swap_a(t_stack **a, t_stack **b);
void			swap_b(t_stack **a, t_stack **b);
void			swap_both(t_stack **a, t_stack **b);
void			push_a(t_stack **a, t_stack **b);
void			push_b(t_stack **a, t_stack **b);
void			rotate_a(t_stack **a, t_stack **b);
void			rotate_b(t_stack **a, t_stack **b);
void			rotate_both(t_stack **a, t_stack **b);
void			reverse_rotate_a(t_stack **a, t_stack **b);
void			reverse_rotate_b(t_stack **a, t_stack **b);
void			reverse_rotate_both(t_stack **a, t_stack **b);

#endif
