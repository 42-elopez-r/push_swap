/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/25 17:51:42 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/29 16:51:58 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHECKER_H
# define CHECKER_H

/*
** INCLUDES:
** stack.h: t_stack
** stdbool.h: bool type
*/

# include <stack.h>
# include <stdbool.h>

bool	operate_stacks(t_stack **a, t_stack **b, bool verbose);

#endif
