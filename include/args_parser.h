/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   args_parser.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/23 19:59:19 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/27 19:03:00 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARGS_PARSER_H
# define ARGS_PARSER_H

/*
** INCLUDES:
** stack.h: t_stack
** stdbool.h: bool type
** stddef.h: size_t type
*/

# include <stack.h>
# include <stdbool.h>
# include <stddef.h>

struct			s_args
{
	int			*nums;
	size_t		nums_len;
	bool		verbose;
};

struct s_args	*parse_args(int argc, char *argv[]);
void			delete_args(struct s_args *args);
t_stack			*init_stack(struct s_args *args);

bool			is_valid_integer(const char *str);
bool			repeated_numbers(struct s_args *args);

#endif
