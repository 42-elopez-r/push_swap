/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/21 18:06:05 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/08 18:17:53 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STACK_H
# define STACK_H

/*
** INCLUDES:
** common.h: struct s_int_array
** stddef.h: size_t type
** stdbool.h: bool type
*/

# include <common.h>
# include <stddef.h>
# include <stdbool.h>

typedef struct s_node
{
	int				num;
	struct s_node	*next;
}					t_stack;

struct s_node		*stack_peek(t_stack *stack);
struct s_node		*stack_index(t_stack *stack, size_t index);
void				stack_pop(t_stack **stack);
void				stack_push(t_stack **stack, int num);
void				delete_stack(t_stack **stack);
size_t				stack_length(t_stack *stack);
size_t				get_top_index(t_stack *stack);
void				stack_swap(t_stack *stack);
void				stack_rotate(t_stack **stack);
void				stack_reverse_rotate(t_stack **stack);
bool				stack_is_sorted(t_stack *stack);
void				display_stacks(t_stack *a, t_stack *b);
size_t				find_smallest_number(t_stack *stack);
size_t				find_biggest_number(t_stack *stack);
struct s_int_array	*stack_to_int_array(t_stack *stack);

#endif
