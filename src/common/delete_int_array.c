/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   delete_int_array.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/02 20:36:29 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/02 20:40:34 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** common.h: This file implements one oof its functions.
** stdlib.h: free()
*/

#include <common.h>
#include <stdlib.h>

/*
** This function frees the memory used by a struct s_int_array.
*/

void	delete_int_array(struct s_int_array *array)
{
	if (array)
	{
		free(array->nums);
		free(array);
	}
}
