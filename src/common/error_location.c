/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_location.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/21 19:57:26 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/25 13:07:23 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** common.h: This file implements one of its functions
** libft.h: ft_put*_fd()
*/

#include <common.h>
#include <libft.h>

/*
** This function prints an error message to stderror with the format:
** "file:line: msg\n".
*/

void	error_location(const char *file, int line, const char *msg)
{
	char	line_str[5];

	res_itoa(line_str, 5, line);
	ft_putendl_fd("Error", 2);
	ft_putstr_fd(file, 2);
	ft_putchar_fd(':', 2);
	ft_putstr_fd(line_str, 2);
	ft_putstr_fd(": ", 2);
	ft_putendl_fd(msg, 2);
}
