/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 13:41:39 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/26 19:12:16 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** common.h: This file implements one of its functions
** libft.h: ft_strlen()
** unistd.h: read()
** stdlib.h: malloc(), free()
*/

#include <common.h>
#include <libft.h>
#include <unistd.h>
#include <stdlib.h>

/*
** This function creates a new string product of concatenating *str and c,
** freeing the previous string and storing the new one in *str.
*/

static int	join_str_chr(char **str, char c)
{
	char	*new;
	size_t	i;
	size_t	str_len;

	str_len = 0;
	if (*str)
		str_len = ft_strlen(*str);
	new = malloc(str_len + 2);
	if (!new)
	{
		error_location(__FILE__, __LINE__, "Failed malloc");
		return (0);
	}
	i = 0;
	while (*str && (*str)[i])
	{
		new[i] = (*str)[i];
		i++;
	}
	new[i++] = c;
	new[i] = '\0';
	free(*str);
	*str = new;
	return (1);
}

/*
** This functions read one line from an open file descriptor and stores it
** in *line. Returns 1 if it read a line, 0 if it reached EOF and -1 in case
** of error.
*/

int	get_next_line(int fd, char **line)
{
	char	buf;
	ssize_t	read_ret;

	buf = '\0';
	*line = NULL;
	while (1)
	{
		read_ret = read(fd, &buf, 1);
		if (read_ret > 0 && buf != '\n')
		{
			if (join_str_chr(line, buf) == 0)
			{
				free(*line);
				return (-1);
			}
			continue ;
		}
		else if (read_ret == 0)
			return (0);
		else if (buf == '\n')
			return (1);
		free(*line);
		return (-1);
	}
	return (-1);
}
