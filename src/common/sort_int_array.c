/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_int_array.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/02 20:43:17 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/02 21:21:18 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** common.h: This function implements one of its functions.
*/

#include <common.h>

/*
** This function looks for the first position of the array in which the
** passed index number is smaller than the number of such position.
*/

static size_t	find_sorted_index(struct s_int_array *array, size_t index)
{
	size_t	i;

	i = 0;
	while (i < index)
	{
		if (array->nums[index] < array->nums[i])
			return (i);
		i++;
	}
	return (index);
}

/*
** This function moves a position's number to a previous position.
** If new_index is bigger than old_index, fasten your seatbelt.
*/

static void	move_back_position(struct s_int_array *array, size_t old_index,
		size_t new_index)
{
	size_t	i;

	i = old_index;
	while (i >= new_index + 1)
	{
		swap_ints(array->nums + i, array->nums + i - 1);
		i--;
	}
}

/*
** This function sorts an struct s_int_array using the insertion algorithm.
*/

void	sort_int_array(struct s_int_array *array)
{
	size_t	i;
	size_t	new_index;

	if (array->length < 2)
		return ;
	i = 1;
	while (i < array->length)
	{
		new_index = find_sorted_index(array, i);
		if (new_index != i)
			move_back_position(array, i, new_index);
		i++;
	}
}
