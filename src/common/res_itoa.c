/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   res_itoa.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 14:47:59 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/26 18:33:24 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** common.h: This function defines one of its functions
** ternary.h: tern_long()
** stddef.h: size_t
*/

#include <common.h>
#include <ternary.h>
#include <stddef.h>

/*
** This function counts the number of characters of an integer.
*/

static size_t	count_chars(long n)
{
	size_t	chars;

	if (n > 0)
		chars = 0;
	else if (n < 0)
	{
		chars = 1;
		n *= -1;
	}
	else
		return (1);
	while (n > 0)
	{
		chars++;
		n /= 10;
	}
	return (chars);
}

/*
** This function is a modification of the Libft ft_itoa(). Instead of
** allocating the string on the heap, it writes the number to a preallocated
** string passed as a parameter, being the parameter max_len the size of 
** that string. If the lenght of the number is too big to fit, it sets the
** string to "".
*/

void	res_itoa(char *str, size_t max_len, int n)
{
	long	n_l;
	size_t	len;

	len = count_chars(n);
	*str = '\0';
	if (len + 1 <= max_len)
	{
		str[len] = '\0';
		len--;
		if (n < 0)
			str[0] = '-';
		n_l = tern_long(n >= 0L, n, n * -1L);
		while (n_l >= 10)
		{
			str[len--] = n_l % 10 + '0';
			n_l /= 10;
		}
		str[len] = n_l + '0';
	}
}
