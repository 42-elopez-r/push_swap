/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reverse_rotates.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/25 20:19:43 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/25 20:25:40 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** instructions.h: This file implements some of its functions.
*/

#include <instructions.h>

/*
** "rra" operation: shift down all elements of stack a by 1. The last element
** becomes the first one.
*/

void	reverse_rotate_a(t_stack **a, t_stack **b)
{
	(void)b;
	stack_reverse_rotate(a);
}

/*
** "rrb" operation: shift down all elements of stack b by 1. The last element
** becomes the first one.
*/

void	reverse_rotate_b(t_stack **a, t_stack **b)
{
	(void)a;
	stack_reverse_rotate(b);
}

/*
** "rrr" operation: shift down all elements of both stacks by 1. The last
** element becomes the first one.
*/

void	reverse_rotate_both(t_stack **a, t_stack **b)
{
	stack_reverse_rotate(a);
	stack_reverse_rotate(b);
}
