/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instructions_1.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/25 18:34:37 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/25 19:05:38 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** instructions.h: This file implements some of its functions.
*/

#include <instructions.h>

/*
** "sa" operation: swaps stack a
*/

void	swap_a(t_stack **a, t_stack **b)
{
	(void)b;
	stack_swap(*a);
}

/*
** "sb" operation: swaps stack b
*/

void	swap_b(t_stack **a, t_stack **b)
{
	(void)a;
	stack_swap(*b);
}

/*
** "ss" operation: swaps both stacks
*/

void	swap_both(t_stack **a, t_stack **b)
{
	stack_swap(*a);
	stack_swap(*b);
}
