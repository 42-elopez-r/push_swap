/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pushes.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/25 19:07:13 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/25 20:26:29 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** instructions.h: This file implements some of its functions.
*/

#include <instructions.h>

/*
** "pa" operation: take the first element at the top of the stack b
** and put it at the top of stack a.
*/

void	push_a(t_stack **a, t_stack **b)
{
	int	num;

	if (!*b)
		return ;
	num = stack_peek(*b)->num;
	stack_pop(b);
	stack_push(a, num);
}

/*
** "pb" operation: take the first element at the top of the stack a
** and put it at the top of stack b.
*/

void	push_b(t_stack **a, t_stack **b)
{
	int	num;

	if (!*a)
		return ;
	num = stack_peek(*a)->num;
	stack_pop(a);
	stack_push(b, num);
}
