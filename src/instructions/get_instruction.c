/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_instruction.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/25 18:15:52 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/01 23:59:33 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** instructions.h: t_instruction, all the instruction functions.
** libft.h: ft_strcmp()
*/

#include <instructions.h>
#include <libft.h>

/*
** This function applys the passed instruction to the stacks. Returns true on
** success, and false if the instruction is not recognized.
*/

t_instruction	get_instruction(const char *instruction)
{
	if (instruction && ft_strcmp(instruction, "sa") == 0)
		return (swap_a);
	else if (instruction && ft_strcmp(instruction, "sb") == 0)
		return (swap_b);
	else if (instruction && ft_strcmp(instruction, "ss") == 0)
		return (swap_both);
	else if (instruction && ft_strcmp(instruction, "pa") == 0)
		return (push_a);
	else if (instruction && ft_strcmp(instruction, "pb") == 0)
		return (push_b);
	else if (instruction && ft_strcmp(instruction, "ra") == 0)
		return (rotate_a);
	else if (instruction && ft_strcmp(instruction, "rb") == 0)
		return (rotate_b);
	else if (instruction && ft_strcmp(instruction, "rr") == 0)
		return (rotate_both);
	else if (instruction && ft_strcmp(instruction, "rra") == 0)
		return (reverse_rotate_a);
	else if (instruction && ft_strcmp(instruction, "rrb") == 0)
		return (reverse_rotate_b);
	else if (instruction && ft_strcmp(instruction, "rrr") == 0)
		return (reverse_rotate_both);
	else
		return (NULL);
}
