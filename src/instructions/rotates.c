/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotates.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/25 20:14:51 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/25 20:26:06 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** instructions.h: This file implements some of its functions.
*/

#include <instructions.h>

/*
** "ra" operation: shift up all elements of stack a by 1. The first element
** becomes the last one.
*/

void	rotate_a(t_stack **a, t_stack **b)
{
	(void)b;
	stack_rotate(a);
}

/*
** "rb" operation: shift up all elements of stack b by 1. The first element
** becomes the last one.
*/

void	rotate_b(t_stack **a, t_stack **b)
{
	(void)a;
	stack_rotate(b);
}

/*
** "rr" operation: shift up all elements of both stacks by 1. The first element
** becomes the last one.
*/

void	rotate_both(t_stack **a, t_stack **b)
{
	stack_rotate(a);
	stack_rotate(b);
}
