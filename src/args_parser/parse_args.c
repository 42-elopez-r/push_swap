/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_args.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/24 17:35:00 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/02 00:37:13 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** args_parser.h: This file implements one of its functions.
** common.h: error_location()
** libft.h: ft_strcmp(), ft_calloc(), ft_atoi()
** ternary.h: tern_int()
** stdlib.h: malloc()
** stdbool.h: bool type
*/

#include <args_parser.h>
#include <common.h>
#include <libft.h>
#include <ternary.h>
#include <stdlib.h>
#include <stdbool.h>

/*
** This function frees the memory of an struct s_arg, sets its pointer to NULL
** and displays an error.
*/

static void	free_args_with_error(struct s_args **args, const char *file,
		int line, const char *error_msg)
{
	delete_args(*args);
	*args = NULL;
	error_location(file, line, error_msg);
}

/*
** This function allocates the nums array from the struct s_args based on
** the number of CLI arguments and wether the verbose field is true or false.
** If there's an error, it sets *args to NULL.
*/

static void	allocate_args_nums(int argc, struct s_args **args)
{
	int	i_argv;

	(*args)->nums = NULL;
	i_argv = tern_int((*args)->verbose, 2, 1);
	if (i_argv >= argc)
	{
		free_args_with_error(args, __FILE__, __LINE__, "Invalid CLI arguments");
		return ;
	}
	(*args)->nums_len = argc - i_argv;
	(*args)->nums = ft_calloc(sizeof(int), (*args)->nums_len);
	if (!(*args)->nums)
	{
		free_args_with_error(args, __FILE__, __LINE__, "Failed malloc");
		return ;
	}
}

/*
** This function receives the CLI arguments and a pointer to a struct s_args
** that must have set the verbose field. It will parse the numbers from
** the CLI arguments and store them in a heap allocated array (nums)
** in the struct. If there's an error, it frees the struct and sets the pointer
** to NULL.
*/

static void	parse_nums(int argc, char *argv[], struct s_args **args)
{
	int	i_argv;
	int	i_nums;

	allocate_args_nums(argc, args);
	if (!*args)
		return ;
	i_argv = tern_int((*args)->verbose, 2, 1);
	i_nums = 0;
	while (i_argv < argc)
	{
		if (is_valid_integer(argv[i_argv]))
			(*args)->nums[i_nums++] = ft_atoi(argv[i_argv]);
		else
		{
			free_args_with_error(args, __FILE__, __LINE__, "Invalid number");
			return ;
		}
		i_argv++;
	}
	if (repeated_numbers(*args))
	{
		free_args_with_error(args, __FILE__, __LINE__, "Repeated numbers");
		return ;
	}
}

/*
** This function receives the CLI arguments and returns a heap allocated
** struct s_args with the parsed data. If there's an error, it returns NULL.
*/

struct s_args	*parse_args(int argc, char *argv[])
{
	struct s_args	*args;

	if (argc <= 1)
	{
		error_location(__FILE__, __LINE__, "No CLI arguments");
		return (NULL);
	}
	args = malloc(sizeof(struct s_args));
	if (!args)
	{
		error_location(__FILE__, __LINE__, "Failed malloc");
		return (NULL);
	}
	args->verbose = ft_strcmp(argv[1], "-v") == 0;
	parse_nums(argc, argv, &args);
	return (args);
}
