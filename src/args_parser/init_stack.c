/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_stack.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/25 17:16:14 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/27 19:23:37 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** args_parser.h: This file implements one of its functions.
** stack.h: stack_push(), t_stack
** sys/types.h: ssize_t
*/

#include <args_parser.h>
#include <stack.h>
#include <sys/types.h>

/*
** This function creates a stack given the passed struct s_args.
*/

t_stack	*init_stack(struct s_args *args)
{
	t_stack	*stack;
	ssize_t	i;

	i = args->nums_len - 1;
	stack = NULL;
	while (i >= 0)
		stack_push(&stack, args->nums[i--]);
	return (stack);
}
