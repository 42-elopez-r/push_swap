/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_valid_integer.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/25 13:01:52 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/28 18:56:28 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** args_parser.h: This file implements one of its functions.
** libft.h: ft_isdigit(), ft_strlen(), ft_atol()
** limits.h: INT_MIN, INT_MAX
*/

#include <args_parser.h>
#include <libft.h>
#include <limits.h>

/*
** This function checks if the passed string isn't empty and is only conformed
** by digits, allowing it also to begin with '-'.
*/

static bool	is_integer(const char *str)
{
	size_t	i;
	bool	not_empty;

	if (!ft_isdigit(*str) && *str != '-')
		return (false);
	not_empty = ft_isdigit(*str);
	i = 1;
	while (str[i])
	{
		if (ft_isdigit(str[i]))
			not_empty = true;
		else
			return (false);
		i++;
	}
	return (not_empty);
}

/*
** Returns true if the number represented in str (because it MUST be a number,
** use the is_integer() function above first) fits into a 4 bytes signed int.
*/

static bool	fits_in_int(const char *str)
{
	long long	n;

	if (*str == '-' && ft_strlen(str) > 11)
		return (false);
	if (*str != '-' && ft_strlen(str) > 10)
		return (false);
	n = ft_atoll(str);
	if (*str == '-')
		return (n >= INT_MIN);
	else
		return (n <= INT_MAX);
}

/*
** This function checks that the passed string is a valid integer of a valid
** size.
*/

bool	is_valid_integer(const char *str)
{
	return (is_integer(str) && fits_in_int(str));
}
