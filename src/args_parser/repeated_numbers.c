/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   repeated_numbers.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/25 16:30:13 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/27 19:07:33 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** args_parser.h: This file implements one of its functions.
*/

#include <args_parser.h>

/*
** This function returns true if there's a duplicate of the especified position
** in the rest of the arguments' array.
*/

static bool	check_position(size_t pos, struct s_args *args)
{
	size_t	i;

	i = 0;
	while (i < args->nums_len)
	{
		if (args->nums[i] == args->nums[pos] && i != pos)
			return (true);
		i++;
	}
	return (false);
}

/*
** This function returns true if there are any repeated numbers in the arguments
*/

bool	repeated_numbers(struct s_args *args)
{
	size_t	i;

	i = 0;
	while (i < args->nums_len)
	{
		if (check_position(i, args))
			return (true);
		i++;
	}
	return (false);
}
