/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   delete_args.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/25 16:50:44 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/25 17:01:50 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** args_parser.h: This file implements one of its functions.
** stdlib.h: free()
*/

#include <args_parser.h>
#include <stdlib.h>

/*
** This function frees the heap allocated memory of a struct s_args.
*/

void	delete_args(struct s_args *args)
{
	if (args)
	{
		free(args->nums);
		free(args);
	}
}
