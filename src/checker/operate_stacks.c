/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operate_stacks.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/25 17:57:12 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/02 00:00:12 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** checker.h: This file implements one of its functions.
** common.h: get_next_line(), error_location()
** instructions.h: t_instruction, get_instruction()
** libft.h: ft_putendl_fd(), ft_putstr_fd(), ft_itoa()
** stdlib.h: free()
*/

#include <checker.h>
#include <common.h>
#include <instructions.h>
#include <libft.h>
#include <stdlib.h>

/*
** This function displays the received instruction.
*/

static void	display_instruction(const char *instruction)
{
	ft_putstr_fd("Instruction: ", 1);
	if (instruction)
		ft_putendl_fd(instruction, 1);
}

/*
** This file displays a wrong instruction error, frees the input and returns
** false. Yes, the norm.
*/

static bool	error_wrong_instruction(char *input)
{
	error_location(__FILE__, __LINE__, "Wrong instruction");
	free(input);
	return (false);
}

/*
** This function displays a counter with the number of instructions received
*/

static void	display_counter(size_t instructions_counter)
{
	char	*str_counter;

	str_counter = ft_itoa(instructions_counter);
	if (!str_counter)
	{
		error_location(__FILE__, __LINE__, "Failed ft_itoa()");
		return ;
	}
	ft_putstr_fd("INSTRUCTIONS COUNT: ", 1);
	ft_putendl_fd(str_counter, 1);
	free(str_counter);
}

/*
** This function reads instructions from stdin and operates them on the stack.
** Returns true if everything went fine, and false if there's an error.
*/

bool	operate_stacks(t_stack **a, t_stack **b, bool verbose)
{
	char			*input;
	t_instruction	instruction;
	size_t			instructions_counter;

	input = NULL;
	instructions_counter = 0;
	while (get_next_line(0, &input) == 1)
	{
		if (verbose)
			display_instruction(input);
		instruction = get_instruction(input);
		if (instruction)
			instruction(a, b);
		else
			return (error_wrong_instruction(input));
		free(input);
		if (verbose)
			display_stacks(*a, *b);
		instructions_counter++;
	}
	if (verbose)
		display_counter(instructions_counter);
	return (true);
}
