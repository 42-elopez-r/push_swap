/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker_main.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/25 17:05:49 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/26 18:44:31 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** checker.h: operate_stacks()
** args_parser.h: parse_args(), delete_args(), struct s_args
** stack.h: delete_stack(), stack_is_sorted(), t_stack
** ternary.h: tern_str(), tern_int()
** libft.h: ft_putendl_fd()
** stdbool.h: bool type
*/

#include <checker.h>
#include <args_parser.h>
#include <stack.h>
#include <libft.h>
#include <ternary.h>
#include <libft.h>
#include <stdbool.h>

/*
** Main function of the Checker program. Returns 0 if the stack ends sorted,
** 1 if the stack doesn't end sorted, and 2 if there's an error.
*/

int	main(int argc, char *argv[])
{
	struct s_args	*args;
	t_stack			*a;
	t_stack			*b;
	bool			ok;

	args = parse_args(argc, argv);
	if (!args)
		return (2);
	a = init_stack(args);
	b = NULL;
	if (!operate_stacks(&a, &b, args->verbose))
	{
		delete_args(args);
		delete_stack(&a);
		delete_stack(&b);
		return (2);
	}
	delete_args(args);
	ok = stack_is_sorted(a) && !b;
	ft_putendl_fd(tern_str(ok, "OK", "KO"), 1);
	delete_stack(&a);
	delete_stack(&b);
	return (tern_int(ok, 0, 1));
}
