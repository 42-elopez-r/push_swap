/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_biggest_number.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/08 17:53:08 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/08 18:51:39 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stack.h>

/*
** This function returns the index (starting at the bottom of the stack)
** of the node with the biggest number.
*/

size_t	find_biggest_number(t_stack *stack)
{
	size_t	index;
	size_t	index_biggest;
	int		biggest;

	index = 0;
	index_biggest = 0;
	biggest = stack->num;
	while (stack)
	{
		if (stack->num > biggest)
		{
			index_biggest = index;
			biggest = stack->num;
		}
		stack = stack->next;
		index++;
	}
	return (index_biggest);
}
