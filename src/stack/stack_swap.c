/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_swap.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 16:53:46 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/22 17:35:52 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** stack.h: This file implements one of its functions
** common.h: swap_ints()
*/

#include <stack.h>
#include <common.h>

/*
** This function swaps the numbers of the two nodes at the top of the stack.
** if the stack has less than two nodes, it does nothing.
*/

void	stack_swap(t_stack *stack)
{
	if (stack_length(stack) < 2)
		return ;
	else
	{
		while (stack->next->next)
			stack = stack->next;
		swap_ints(&stack->num, &stack->next->num);
	}
}
