/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_get_top_index.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:58:40 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/06 19:02:21 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** stack.h: This function implements one of its functions.
** ternary.h: tern_size_t()
*/

#include <stack.h>
#include <ternary.h>

/*
** This function returns the index of the top of the stack.
*/

size_t	get_top_index(t_stack *stack)
{
	size_t	length;

	length = stack_length(stack);
	return (tern_size_t(length > 0, length - 1, 0));
}
