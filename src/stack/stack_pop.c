/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_pop.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 17:43:08 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/26 18:40:18 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** stack.h: This file implements one of its functions
** stdlib.h: free()
*/

#include <stack.h>
#include <stdlib.h>

/*
** This function deletes the node at the top of the stack.
*/

void	stack_pop(t_stack **stack)
{
	struct s_node	*prev;
	struct s_node	*cur;

	if (!stack || !*stack)
		return ;
	prev = NULL;
	cur = *stack;
	while (cur->next)
	{
		prev = cur;
		cur = cur->next;
	}
	if (prev)
		prev->next = NULL;
	else
		*stack = NULL;
	free(cur);
}
