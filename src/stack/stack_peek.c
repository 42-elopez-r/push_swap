/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_peek.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/21 19:34:22 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/21 19:54:16 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** stack.h: This file implements one of its functions.
*/

#include <stack.h>

/*
** This functions returns the node at the top of the stack. If the stack is
** empty returns NULL.
*/

struct s_node	*stack_peek(t_stack *stack)
{
	if (!stack)
		return (NULL);
	while (stack->next)
		stack = stack->next;
	return (stack);
}
