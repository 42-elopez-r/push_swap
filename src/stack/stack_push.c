/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_push.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/21 18:26:14 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/26 18:39:45 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** stack.h: This file implements one of its functions
** common.h: error_location()
** stdlib.h: malloc()
*/

#include <stack.h>
#include <common.h>
#include <stdlib.h>

/*
** This function sets the new node at the top of the stack and assings it the
** specified number.
*/

static void	non_empty_stack_push(t_stack *stack, struct s_node *new, int num)
{
	struct s_node	*prev;

	prev = stack_peek(stack);
	new->num = num;
	new->next = NULL;
	prev->next = new;
}

/*
** This function sets the new node as the stack and assings it the specified
** number.
*/

static void	empty_stack_push(t_stack **stack, struct s_node *new, int num)
{
	new->num = num;
	new->next = NULL;
	*stack = new;
}

/*
** This function adds a new node to the top of the stack with the specified
** number.
*/

void	stack_push(t_stack **stack, int num)
{
	struct s_node	*new;

	if (!stack)
	{
		error_location(__FILE__, __LINE__, "NULL stack");
		return ;
	}
	new = malloc(sizeof(struct s_node));
	if (!new)
	{
		error_location(__FILE__, __LINE__, "Failed malloc");
		return ;
	}
	if (*stack)
		non_empty_stack_push(*stack, new, num);
	else
		empty_stack_push(stack, new, num);
}
