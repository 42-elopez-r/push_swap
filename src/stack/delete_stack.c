/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   delete_stack.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 16:35:55 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/22 16:45:22 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES;
** stack.h: This file implements one of its functions.
** common.h: error_location()
** stdlib.h: free()
*/

#include <stack.h>
#include <common.h>
#include <stdlib.h>

/*
** This function receives a stack and frees all of its nodes, setting the
** then the stack to NULL.
*/

void	delete_stack(t_stack **stack)
{
	struct s_node	*cur;
	struct s_node	*next;

	if (!stack)
	{
		error_location(__FILE__, __LINE__, "NULL stack");
		return ;
	}
	cur = *stack;
	while (cur)
	{
		next = cur->next;
		free(cur);
		cur = next;
	}
	*stack = NULL;
}
