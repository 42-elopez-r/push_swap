/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_rotate.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 18:00:12 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/02 00:31:54 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** stack.h: This file implements one of its functions
** common.h: error_location()
** stdlib.h: malloc()
*/

#include <stack.h>
#include <common.h>
#include <stdlib.h>

/*
** This function rotates the whole stack making the last node be the first one.
*/

void	stack_rotate(t_stack **stack)
{
	struct s_node	*new_bottom;

	if (!*stack || !(*stack)->next)
		return ;
	new_bottom = malloc(sizeof(struct s_node));
	if (!new_bottom)
	{
		error_location(__FILE__, __LINE__, "Failed malloc()");
		return ;
	}
	new_bottom->num = stack_peek(*stack)->num;
	stack_pop(stack);
	new_bottom->next = *stack;
	*stack = new_bottom;
}
