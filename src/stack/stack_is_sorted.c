/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_is_sorted.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/25 21:14:52 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/26 19:16:51 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** stack.h: This file implements one of its functions.
*/

#include <stack.h>

/*
** This function returns true if the passed stack is sorted and non empty.
*/

bool	stack_is_sorted(t_stack *stack)
{
	int	prev_num;

	if (!stack)
		return (false);
	prev_num = stack->num;
	stack = stack->next;
	while (stack)
	{
		if (stack->num > prev_num)
			return (false);
		prev_num = stack->num;
		stack = stack->next;
	}
	return (true);
}
