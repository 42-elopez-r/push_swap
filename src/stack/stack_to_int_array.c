/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_to_int_array.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/02 20:15:13 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/02 21:21:57 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** stack.h: This file implements one of its functions.
** common.h: error_location()
** libft.h: ft_calloc()
** stdlib.h: malloc(), free()
*/

#include <stack.h>
#include <common.h>
#include <libft.h>
#include <stdlib.h>

/*
** This function writes the numbers from the passed stack into the passed
** array.
*/

static void	write_to_array(t_stack *stack, struct s_int_array *array)
{
	size_t	i;

	i = 0;
	while (stack)
	{
		array->nums[i++] = stack->num;
		stack = stack->next;
	}
}

/*
** This function allocates and fills a s_int_array copying the numbers from
** the passed stack. Returns NULL in case of error.
*/

struct s_int_array	*stack_to_int_array(t_stack *stack)
{
	struct s_int_array	*array;

	array = malloc(sizeof(struct s_int_array));
	if (!array)
	{
		error_location(__FILE__, __LINE__, "Failed malloc()");
		return (NULL);
	}
	array->length = stack_length(stack);
	array->nums = ft_calloc(sizeof(int), array->length);
	if (!array->nums)
	{
		error_location(__FILE__, __LINE__, "Failed malloc()");
		free(array);
		return (NULL);
	}
	write_to_array(stack, array);
	return (array);
}
