/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_stacks.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/25 20:43:57 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/29 16:53:53 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** stack.h: This file implements one of its functions.
** libft.h: ft_itoa(), ft_putstr_fd(), ft_putendl_fd()
** stdlib.h: free()
*/

#include <stack.h>
#include <libft.h>
#include <stdlib.h>

/*
** This function displays to stdout the numbers of a stack, starting at the top.
*/

static void	display_stack(t_stack *stack)
{
	char	*num_str;

	if (!stack)
	{
		ft_putstr_fd("(empty)", 1);
		return ;
	}
	if (stack->next)
		display_stack(stack->next);
	num_str = ft_itoa(stack->num);
	ft_putstr_fd(num_str, 1);
	ft_putchar_fd(' ', 1);
	free(num_str);
}

/*
** This function displays the current state of both stacks to stdout.
*/

void	display_stacks(t_stack *a, t_stack *b)
{
	ft_putstr_fd("a: ", 1);
	display_stack(a);
	ft_putendl_fd("", 1);
	ft_putstr_fd("b: ", 1);
	display_stack(b);
	ft_putendl_fd("", 1);
}
