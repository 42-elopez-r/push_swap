/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_reverse_rotate.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 19:32:43 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/02 00:33:01 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** stack.h: This file implements one of its functions.
*/

#include <stack.h>

/*
** This function rotates the whole stack making the first node be the last one.
*/

void	stack_reverse_rotate(t_stack **stack)
{
	struct s_node	*new_top;

	if (!*stack || !(*stack)->next)
		return ;
	new_top = *stack;
	stack_peek(*stack)->next = new_top;
	*stack = (*stack)->next;
	new_top->next = NULL;
}
