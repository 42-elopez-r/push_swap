/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_length.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 16:48:18 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/22 16:52:10 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** stack.h: This file implements one of its functions
*/

#include <stack.h>

/*
** This function receives a pointer to a stack and returns the number of
** nodes it contains.
*/

size_t	stack_length(t_stack *stack)
{
	size_t	length;

	length = 0;
	while (stack)
	{
		length++;
		stack = stack->next;
	}
	return (length);
}
