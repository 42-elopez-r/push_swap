/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_index.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/03 19:43:42 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/03 19:47:22 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** stack.h: This file implements one of its functions.
*/

#include <stack.h>

/*
** This function returns the node of a stack corresponding to the passed index
** (bottom is index 0). Returns NULL if the index is too big.
*/

struct s_node	*stack_index(t_stack *stack, size_t index)
{
	size_t	i;

	i = 0;
	while (stack)
	{
		if (i == index)
			return (stack);
		i++;
		stack = stack->next;
	}
	return (NULL);
}
