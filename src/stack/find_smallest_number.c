/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_smallest_number.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/28 21:12:27 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/28 21:23:33 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stack.h>

/*
** This function returns the index (starting at the bottom of the stack)
** of the node with the smallest number.
*/

size_t	find_smallest_number(t_stack *stack)
{
	size_t	index;
	size_t	index_smallest;
	int		smallest;

	index = 0;
	index_smallest = 0;
	smallest = stack->num;
	while (stack)
	{
		if (stack->num < smallest)
		{
			index_smallest = index;
			smallest = stack->num;
		}
		stack = stack->next;
		index++;
	}
	return (index_smallest);
}
