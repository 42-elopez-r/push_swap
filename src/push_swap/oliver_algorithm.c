/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   oliver_algorithm.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/28 21:02:53 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/08 18:58:31 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** push_swap.h: This file implements one of its functions.
** instructions.h: push_b()
** libft.h: ft_putendl_fd()
** sys/types.h: ssize_t type.
*/

#include <push_swap.h>
#include <instructions.h>
#include <libft.h>
#include <sys/types.h>

/*
** This function does pushes from b to a until b is empty.
*/

static void	empty_stack_b(t_stack **a, t_stack **b, bool verbose)
{
	while (*b)
	{
		push_a(a, b);
		ft_putendl_fd("pa", 1);
		if (verbose)
			display_stacks(*a, *b);
	}
}

/*
** This function implements the algorithm I've called "Oliver" (which may or
** may not exist already, dunno). It goes looking the smallest number on the
** stack a and pushing it to the stack b until only one node remains on a,
** and then it pushes all the nodes (already reverse sorted) back to a.
*/

void	oliver_algorithm(t_stack **a, t_stack **b, bool verbose)
{
	size_t	a_length;
	size_t	smallest_index;

	a_length = stack_length(*a);
	while (a_length > 1)
	{
		smallest_index = find_smallest_number(*a);
		if (smallest_index > a_length / 2 && smallest_index != a_length - 1)
			move_a_to_top(a, b, smallest_index, verbose);
		else if (smallest_index <= a_length / 2)
			reverse_move_a_to_top(a, *b, smallest_index, verbose);
		push_b(a, b);
		ft_putendl_fd("pb", 1);
		if (verbose)
			display_stacks(*a, *b);
		a_length = stack_length(*a);
	}
	empty_stack_b(a, b, verbose);
}
