/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_size_five.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/20 20:21:36 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/22 20:46:37 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** push_swap.h: This file implements one of its functions.
** instructions.h: push_b(), rotate_a(), push_a()
** sys/types.h: ssize_t type.
** ternary.h: tern_size_t()
*/

#include <push_swap.h>
#include <instructions.h>
#include <sys/types.h>
#include <ternary.h>

/*
** This function rotates the stack a so the smallest number ends at the top
** of the stack.
*/

static void	rotate_a_smallest_to_top(t_stack **a, t_stack **b, bool verbose)
{
	size_t	smallest_index;
	size_t	a_len;

	smallest_index = find_smallest_number(*a);
	a_len = stack_length(*a);
	if (smallest_index > a_len / 2)
		move_a_to_top(a, b, smallest_index, verbose);
	else
		reverse_move_a_to_top(a, *b, smallest_index, verbose);
}

/*
** This function rotates the stack a so top_b will be pushed later on the
** right place.
*/

static void	prepare_stack_a(int top_b, t_stack **a, t_stack **b, bool verbose)
{
	ssize_t	smallest_index;
	ssize_t	a_length;
	ssize_t	i;
	bool	first_iteration;

	a_length = stack_length(*a);
	smallest_index = find_smallest_number(*a);
	i = smallest_index;
	first_iteration = true;
	while (true)
	{
		if (stack_index(*a, i)->num > top_b
			|| (!first_iteration && i == smallest_index))
			break ;
		if (--i == -1)
		{
			i = a_length - 1;
			first_iteration = false;
		}
	}
	if (i >= a_length / 2 && i != a_length - 1)
		move_a_to_top(a, b, i, verbose);
	else if (i < a_length / 2)
		reverse_move_a_to_top(a, *b, i, verbose);
}

/*
** This algorithm sorts a stack of five numbers by pushing two of them to the
** stack b, then sorting the three remaining in the stack a with the
** sort_size_three() algorithm and finally inserting the two numbers of the
** stack b in the right place of the stack a.
*/

void	sort_size_five(t_stack **a, t_stack **b, bool verbose)
{
	push_b(a, b);
	display_instruction("pb", verbose, *a, *b);
	push_b(a, b);
	display_instruction("pb", verbose, *a, *b);
	sort_size_three(a, b, verbose);
	prepare_stack_a(stack_peek(*b)->num, a, b, verbose);
	push_a(a, b);
	display_instruction("pa", verbose, *a, *b);
	prepare_stack_a(stack_peek(*b)->num, a, b, verbose);
	push_a(a, b);
	display_instruction("pa", verbose, *a, *b);
	rotate_a_smallest_to_top(a, b, verbose);
}
