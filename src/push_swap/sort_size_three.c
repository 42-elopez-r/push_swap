/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_size_three.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/22 16:48:25 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/22 17:14:31 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** push_swap.h: This file implements one of its functions.
** instructions.h: swap_a(), reverse_rotate_a(), rotate_a()
*/

#include <push_swap.h>
#include <instructions.h>

/*
** Sequel of sort_size_three()'s if-else chain.
*/

static void	sort_size_three_and_the_secrets_chamber(t_stack **a, t_stack **b,
		bool verbose)
{
	if (stack_index(*a, 2)->num < stack_index(*a, 1)->num
		&& stack_index(*a, 1)->num > stack_index(*a, 0)->num
		&& stack_index(*a, 2)->num < stack_index(*a, 0)->num)
	{
		swap_a(a, b);
		display_instruction("sa", verbose, *a, *b);
		rotate_a(a, b);
		display_instruction("ra", verbose, *a, *b);
	}
	else
	{
		reverse_rotate_a(a, b);
		display_instruction("rra", verbose, *a, *b);
	}
}

/*
** This function sorts the stack a if it has three numbers by checking
** which one of all the possible combinations is.
*/

void	sort_size_three(t_stack **a, t_stack **b, bool verbose)
{
	if (stack_is_sorted(*a))
		return ;
	if (stack_index(*a, 2)->num > stack_index(*a, 1)->num
		&& stack_index(*a, 2)->num < stack_index(*a, 0)->num)
	{
		swap_a(a, b);
		display_instruction("sa", verbose, *a, *b);
	}
	else if (stack_index(*a, 2)->num > stack_index(*a, 1)->num
		&& stack_index(*a, 1)->num > stack_index(*a, 0)->num)
	{
		swap_a(a, b);
		display_instruction("sa", verbose, *a, *b);
		reverse_rotate_a(a, b);
		display_instruction("rra", verbose, *a, *b);
	}
	else if (stack_index(*a, 2)->num > stack_index(*a, 1)->num
		&& stack_index(*a, 1)->num < stack_index(*a, 0)->num)
	{
		rotate_a(a, b);
		display_instruction("ra", verbose, *a, *b);
	}
	else
		sort_size_three_and_the_secrets_chamber(a, b, verbose);
}
