/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap_main.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/28 20:33:19 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/03 14:05:41 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** push_swap.h: oliver_algorithm()
** args_parser.h: parse_args(), delete_args()
** common.h: error_location()
*/

#include <push_swap.h>
#include <args_parser.h>
#include <common.h>

int	main(int argc, char *argv[])
{
	struct s_args	*args;
	t_stack			*a;
	t_stack			*b;

	args = parse_args(argc, argv);
	if (!args)
		return (2);
	a = init_stack(args);
	b = NULL;
	if (ALGORITHM == 1)
		oliver_algorithm(&a, &b, args->verbose);
	else if (ALGORITHM == 2)
		chunk_algorithm(&a, &b, args->verbose);
	else
		error_location(__FILE__, __LINE__, "No sorting algorithm set");
	delete_args(args);
	delete_stack(&a);
	delete_stack(&b);
	return (0);
}
