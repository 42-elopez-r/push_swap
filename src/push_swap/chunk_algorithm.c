/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chunk_algorithm.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/03 13:38:09 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/27 18:15:27 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** push_swap.h: This file implements one of its functions.
** stack.h: stack_to_int_array(), display_stacks(), stack_is_sorted()
** common.h: sort_int_array(), delete_int_array()
** instructions.h: push_b()
*/

#include <push_swap.h>
#include <stack.h>
#include <common.h>
#include <instructions.h>

/*
** This function receives the stack a and returns an struct s_int_array with the
** stack numbers sorted. Returns NULL on error.
*/

static struct s_int_array	*get_sorted_array(t_stack *a)
{
	struct s_int_array	*sorted;

	sorted = stack_to_int_array(a);
	if (!sorted)
		return (NULL);
	sort_int_array(sorted);
	return (sorted);
}

/*
 * This function performs the insertion algorithm with the amount of chunks
 * specified.
 */

static void	sort_by_chunks(t_stack **a, t_stack **b, bool verbose,
		int num_chunks)
{
	struct s_int_array	*sorted;
	int					chunk;
	ssize_t				selected_index;

	sorted = get_sorted_array(*a);
	if (!sorted)
		return ;
	chunk = 0;
	while (chunk < num_chunks)
	{
		selected_index = search_chunk(*a, sorted, num_chunks, chunk);
		if (selected_index == -1)
		{
			chunk++;
			continue ;
		}
		if ((size_t)selected_index <= stack_length(*a) / 2)
			reverse_move_a_to_top(a, *b, selected_index, verbose);
		else
			move_a_to_top(a, b, selected_index, verbose);
		push_to_b(a, b, verbose);
	}
	delete_int_array(sorted);
	rotate_empty_stack_b(a, b, verbose);
}

void	chunk_algorithm(t_stack **a, t_stack **b, bool verbose)
{
	size_t	a_len;

	if (stack_is_sorted(*a))
		return ;
	a_len = stack_length(*a);
	if (a_len == 3)
		sort_size_three(a, b, verbose);
	else if (a_len == 5)
		sort_size_five(a, b, verbose);
	else if (a_len <= 100)
		sort_by_chunks(a, b, verbose, 5);
	else
		sort_by_chunks(a, b, verbose, 12);
}
