/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_to_b.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/06 18:45:32 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/11 23:43:27 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** push_swap.h: This file implements one of its functions.
** stack.h: stack_peek(), stack_length(), stack_index(), find_smallest_number()
** instructions.h: push_b()
** ternary.h: tern_size_t()
*/

#include <push_swap.h>
#include <stack.h>
#include <instructions.h>
#include <ternary.h>

/*
** This function rotates the stack b so top_a will be pushed later on the
** right place.
*/

static void	prepare_stack_b(int top_a, t_stack **a, t_stack **b, bool verbose)
{
	size_t	smallest_index;
	size_t	b_length;
	size_t	i;
	bool	first_iteration;

	b_length = stack_length(*b);
	smallest_index = find_smallest_number(*b);
	i = smallest_index;
	first_iteration = true;
	while (true)
	{
		if (stack_index(*b, i)->num > top_a
			|| (!first_iteration && i == smallest_index))
			break ;
		if (++i == b_length)
		{
			i = 0;
			first_iteration = false;
		}
	}
	i = tern_size_t(i, i - 1, b_length - 1);
	if (i > b_length / 2 && i != b_length - 1)
		move_b_to_top(a, b, i, verbose);
	else if (i <= b_length / 2)
		reverse_move_b_to_top(*a, b, i, verbose);
}

/*
** This function rotates pushes the top of the stack a to the stack b, but
** before doing so it rotates the stack b to put the right number before the
** one that's going to be pushed.
*/

void	push_to_b(t_stack **a, t_stack **b, bool verbose)
{
	if (stack_length(*b) > 1)
		prepare_stack_b(stack_peek(*a)->num, a, b, verbose);
	push_b(a, b);
	display_instruction("pb", verbose, *a, *b);
}
