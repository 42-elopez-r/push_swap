/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate_empty_stack_b.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/08 17:50:57 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/10 13:18:54 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** push_swap.h: This file implements one of its functions.
** instructions.h: push_a()
*/

#include <push_swap.h>
#include <instructions.h>

/*
** Pushes all the numbers from the stack b to the stack a in the right order.
*/

void	rotate_empty_stack_b(t_stack **a, t_stack **b, bool verbose)
{
	size_t	biggest_index;
	size_t	b_length;

	while (*b)
	{
		b_length = stack_length(*b);
		biggest_index = find_biggest_number(*b);
		if (biggest_index > b_length / 2 && biggest_index != b_length - 1)
			move_b_to_top(a, b, biggest_index, verbose);
		else if (biggest_index <= b_length / 2)
			reverse_move_b_to_top(*a, b, biggest_index, verbose);
		push_a(a, b);
		display_instruction("pa", verbose, *a, *b);
	}
}
