/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_to_top.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/05 18:59:23 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/10 15:48:35 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** push_swap.h: This file implements one of its functions.
** stack.h: display_stacks()
** instructions.h: rotate_a(), reverse_rotate_a(), rotate_b(),
**                 reverse_rotate_b()
*/

#include <push_swap.h>
#include <stack.h>
#include <instructions.h>

/*
** This function rotates downwards the stack b until the passed index (0 is the
** bottom of the stack) is on the top of the stack.
*/

void	reverse_move_b_to_top(t_stack *a, t_stack **b, ssize_t index,
		bool verbose)
{
	while (index > -1)
	{
		reverse_rotate_b(NULL, b);
		display_instruction("rrb", verbose, a, *b);
		index--;
	}
}

/*
** This function rotates upwards the stack b until the passed index (0 is the
** bottom of the stack) is on the top of the stack.
*/

void	move_b_to_top(t_stack **a, t_stack **b, size_t index, bool verbose)
{
	size_t	b_length;

	b_length = stack_length(*b);
	while (++index < b_length)
	{
		rotate_b(a, b);
		display_instruction("rb", verbose, *a, *b);
	}
}

/*
** This function rotates downwards the stack a until the passed index (0 is the
** bottom of the stack) is on the top of the stack.
*/

void	reverse_move_a_to_top(t_stack **a, t_stack *b, ssize_t index,
		bool verbose)
{
	while (index > -1)
	{
		reverse_rotate_a(a, NULL);
		display_instruction("rra", verbose, *a, b);
		index--;
	}
}

/*
** This function rotates upwards the stack a until the passed index (0 is the
** bottom of the stack) is on the top of the stack.
*/

void	move_a_to_top(t_stack **a, t_stack **b, size_t index, bool verbose)
{
	size_t	a_length;

	a_length = stack_length(*a);
	while (++index < a_length)
	{
		rotate_a(a, b);
		display_instruction("ra", verbose, *a, *b);
	}
}
