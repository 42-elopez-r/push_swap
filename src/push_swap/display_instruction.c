/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_instruction.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/04 18:35:48 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/04 18:42:49 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** push_swap.h: This file implements one of its functions.
** stack.h: display_stacks()
** libft.h: ft_putendl_fd()
** stdbool.h: bool type
*/

#include <push_swap.h>
#include <stack.h>
#include <libft.h>
#include <stdbool.h>

/*
** This function displays the instruction passed and the state of the stacks
** if the verbose mode is enabled.
*/

void	display_instruction(const char *instruction, bool verbose,
		t_stack *a, t_stack *b)
{
	ft_putendl_fd(instruction, 1);
	if (verbose)
		display_stacks(a, b);
}
