/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search_chunk.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/03 16:21:39 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/11 22:24:16 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** push_swap.h: This file implements one of its functions
** ternary.h: tern_int()
** stdbool.h: bool type
*/

#include <push_swap.h>
#include <ternary.h>
#include <stdbool.h>

/*
** This function checks if the passed number belongs to the given chunk.
*/

static bool	is_number_in_chunk(int n, struct s_int_array *sorted, size_t min,
		size_t max)
{
	size_t	i;

	i = min;
	while (i < sorted->length && i <= max)
	{
		if (sorted->nums[i] == n)
			return (true);
		i++;
	}
	return (false);
}

/*
** This function returns the index of the first occurence of a number of the
** given chunk looking from top to bottom.
*/

static ssize_t	get_index_from_top(t_stack *stack, struct s_int_array *sorted,
		size_t min_i_chunk, size_t max_i_chunk)
{
	ssize_t	i;

	i = stack_length(stack) - 1;
	while (i >= 0)
	{
		if (is_number_in_chunk(stack_index(stack, i)->num, sorted,
				min_i_chunk, max_i_chunk))
			return (i);
		i--;
	}
	return (-1);
}

/*
** This function returns the index of the first occurence of a number of the
** given chunk looking from bottom to top.
*/

static ssize_t	get_index_from_bottom(t_stack *stack,
		struct s_int_array *sorted, size_t min_i_chunk, size_t max_i_chunk)
{
	size_t	i;

	i = 0;
	while (stack)
	{
		if (is_number_in_chunk(stack->num, sorted, min_i_chunk, max_i_chunk))
			return (i);
		i++;
		stack = stack->next;
	}
	return (-1);
}

/*
** This function searches for the index most convenient to move to the top that
** belongs to a certain chunk. Returns -1 if there are no numbers of that chunk
** in the stack.
*/

ssize_t	search_chunk(t_stack *stack, struct s_int_array *sorted, int n_chunks,
		int chunk)
{
	size_t	min_i_chunk;
	size_t	max_i_chunk;
	ssize_t	index_from_top;
	ssize_t	index_from_bottom;
	size_t	chunk_size;

	chunk_size = sorted->length / n_chunks + tern_int(sorted->length % n_chunks,
			1, 0);
	min_i_chunk = chunk_size * chunk;
	max_i_chunk = min_i_chunk + chunk_size - 1;
	index_from_bottom = get_index_from_bottom(stack, sorted, min_i_chunk,
			max_i_chunk);
	if (index_from_bottom == -1)
		return (-1);
	index_from_top = get_index_from_top(stack, sorted, min_i_chunk,
			max_i_chunk);
	if (stack_length(stack) - 1 - index_from_top < (size_t)index_from_bottom)
		return (index_from_top);
	else
		return (index_from_bottom);
}
