# Algorithm selector:
# 1: Oliver
# 2: Chunk
ALGORITHM=2

CC = cc
SHELL = /bin/sh
CFLAGS += -Iinclude -Wall -Werror -Wextra -D ALGORITHM=$(ALGORITHM)

NAME_PUSH_SWAP = push_swap
NAME_CHECKER = checker
NAME_TESTER = tester

COMMON_FOLDERS = src/args_parser src/common src/instructions src/libft src/stack src/ternary

SRCS_PUSH_SWAP:= $(shell find $(COMMON_FOLDERS) src/push_swap -name "*.c")
OBJS_PUSH_SWAP = $(SRCS_PUSH_SWAP:.c=.o)

SRCS_CHECKER:= $(shell find $(COMMON_FOLDERS) src/checker -name "*.c")
OBJS_CHECKER = $(SRCS_CHECKER:.c=.o)

SRCS_BONUS:= $(shell find bonus -name "*.c")
OBJS_BONUS = $(SRCS_BONUS:.c=.o)

all: $(NAME_CHECKER) $(NAME_PUSH_SWAP)

$(NAME_PUSH_SWAP): $(OBJS_PUSH_SWAP)
	$(CC) $(CFLAGS) -o $(NAME_PUSH_SWAP) $(OBJS_PUSH_SWAP)

$(NAME_CHECKER): $(OBJS_CHECKER)
	$(CC) $(CFLAGS) -o $(NAME_CHECKER) $(OBJS_CHECKER)

.o: .c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	find . -name "*.o" -delete -print

fclean: clean
	rm -f $(NAME_CHECKER) $(NAME_PUSH_SWAP) $(NAME_TESTER)

re: fclean all

bonus: all $(OBJS_BONUS)
	$(CC) $(CFLAGS) -o $(NAME_TESTER) $(OBJS_BONUS)

debug: CFLAGS += -g
debug: fclean bonus

.PHONY: all clean fclean re bonus debug
