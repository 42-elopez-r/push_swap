/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tester_delete_array.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/01 20:21:56 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/01 20:31:17 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** tester.h: This file implements one of its functions.
** stdlib.h: free()
*/

#include "tester.h"
#include <stdlib.h>

/*
** This function frees the memory of the passed NULL terminated array of strings
*/

void	delete_array(char **str_numbers)
{
	size_t	i;

	i = 0;
	if (str_numbers)
	{
		while (str_numbers[i])
			free(str_numbers[i++]);
		free(str_numbers);
	}
}
