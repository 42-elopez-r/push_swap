/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tester_generate_array.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/30 20:06:11 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/01 20:34:39 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** tester.h: This file implements one of its functions.
** stdbool.h: bool type
** stdio.h: fputs(), snprintf(), sprintf()
** stdlib.h: calloc(), free(), rand()
*/

#include "tester.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/*
** This function checks if a number is already present (returns true) in the
** given array checking until a given index.
*/

static bool	is_repeated(int *numbers, size_t index, int number)
{
	size_t	i;

	i = 0;
	while (i < index)
	{
		if (numbers[i++] == number)
			return (true);
	}
	return (false);
}

/*
** This function takes an array of numbers and returns an array of strings
** representing the numbers. Returns NULL in case of error.
*/

static char	**convert_to_str_array(int *numbers, size_t size)
{
	char	**str_numbers;
	size_t	i;
	char	*str_number;

	str_numbers = calloc(sizeof(char *), size + 1);
	if (!str_numbers)
		return (NULL);
	i = 0;
	while (i < size)
	{
		str_number = calloc(sizeof(char), snprintf(NULL, 0, "%d ", numbers[i]));
		sprintf(str_number, "%d", numbers[i]);
		str_numbers[i++] = str_number;
	}
	return (str_numbers);
}

/*
** This function generates an array of random unrepeated numbers, as strings,
** of the given size and between the min and max range. Returns NULL in case
** of error.
*/

char	**generate_array(size_t size, int min, int max)
{
	int		current;
	int		*numbers;
	size_t	i;
	char	**str_numbers;

	if (size > (size_t)(max - min))
	{
		fputs("The range for number generation is too short for the size set\n",
			stderr);
		return (NULL);
	}
	numbers = calloc(sizeof(int), size);
	if (!numbers)
		return (NULL);
	i = 0;
	while (i < size)
	{
		current = min + rand() % (max - min + 1);
		while (is_repeated(numbers, i, current))
			current = min + rand() % (max - min + 1);
		numbers[i++] = current;
	}
	str_numbers = convert_to_str_array(numbers, size);
	free(numbers);
	return (str_numbers);
}
