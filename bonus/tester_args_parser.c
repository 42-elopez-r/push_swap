/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tester_args_parser.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/29 20:01:08 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/01 21:01:01 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDE:
** stdio.h: fputs(), perror()
** string.h: strcmp()
** stdlib.h: calloc(), free(), atol()
*/

#include "tester.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
** This function receives the CLI arguments with an offset (to skip the
** push_swap option) with the allocated struct s_tester_args. It will free the
** struct and return NULL if the CLI arguments left unparsed are less than three
** Otherwise it parses the checker executable and generation range returning
** the struct.
*/

struct s_tester_args	*parse_mandatory_args(int argc, char *argv[],
							int offset, struct s_tester_args *args)
{
	if (argc - offset < 5)
	{
		fputs("Use: tester [-p push_swap ] checker SIZE MIN MAX\n", stderr);
		free(args);
		return (NULL);
	}
	args->checker_exe = argv[offset + 1];
	args->size = atol(argv[offset + 2]);
	args->min = atoi(argv[offset + 3]);
	args->max = atoi(argv[offset + 4]);
	return (args);
}

/*
** This function parses the CLI arguments returning a struct s_tester_args, or
** returns NULL when there's an error.
*/

struct s_tester_args	*parse_tester_arguments(int argc, char *argv[])
{
	struct s_tester_args	*args;
	int						offset;

	if (argc != 5 && argc != 7)
	{
		fputs("Use: tester [-p push_swap ] checker SIZE MIN MAX\n", stderr);
		return (NULL);
	}
	args = calloc(sizeof(struct s_tester_args), 1);
	if (!args)
	{
		perror("Can't allocate struct s_tester_args");
		return (NULL);
	}
	offset = 0;
	if (strcmp(argv[1], "-p") == 0)
	{
		args->push_swap_exe = argv[2];
		offset = 2;
	}
	return (parse_mandatory_args(argc, argv, offset, args));
}
