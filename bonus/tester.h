/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tester.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/28 19:15:51 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/22 20:42:04 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TESTER_H
# define TESTER_H

/*
** INLUDES:
** stddef.h: size_t type
*/

# include <stddef.h>

struct s_tester_args
{
	char	*push_swap_exe;
	char	*checker_exe;
	size_t	size;
	int		min;
	int		max;
};

struct s_tester_args	*parse_tester_arguments(int argc, char *argv[]);
char					**generate_array(size_t size, int min, int max);
char					**bottom_append_string_array(char **array,
							char *append);
int						launch_subprocesses(struct s_tester_args *args,
							char **stack_array);
void					display_stack(char **stack_array);
void					delete_array(char **str_numbers);

#endif
