/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tester_main.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/28 19:24:39 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/22 20:41:35 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** tester.h: parse_tester_arguments(), generate_array()
** stdlib.h: srand(), free()
** time.h: time()
*/

#include "tester.h"
#include <stdlib.h>
#include <time.h>

int	main(int argc, char *argv[])
{
	struct s_tester_args	*args;
	char					**stack_array;
	int						test_ok;

	srand(time(NULL));
	args = parse_tester_arguments(argc, argv);
	if (!args)
		return (2);
	stack_array = generate_array(args->size, args->min, args->max);
	if (!stack_array)
	{
		free(args);
		return (2);
	}
	display_stack(stack_array);
	test_ok = launch_subprocesses(args, stack_array);
	free(args);
	delete_array(stack_array);
	return (test_ok);
}
