/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tester_launch_subprocesses.c                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/01 17:54:41 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/22 20:45:24 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** tester.h: This file implements one of its functions.
** unistd.h: pipe(), close(), dup2(), fork(), execv()
** stdio.h: perror()
** sys/wait.h: waitpid()
** stdlib.h: exit()
*/

#include "tester.h"
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

/*
** This function launches the push_swap process, returning its pid. It
** requires the pipe to write to.
*/

static pid_t	launch_push_swap(char *push_swap_exe, char **stack_array,
		int pipefds[2])
{
	pid_t	pid;
	char	**argv;

	pid = fork();
	if (pid == -1)
		perror("Can't spawn push_swap process");
	else if (pid == 0)
	{
		argv = bottom_append_string_array(stack_array, push_swap_exe);
		close(pipefds[0]);
		dup2(pipefds[1], 1);
		execv(push_swap_exe, argv);
		perror("Can't launch push_swap");
		exit(1);
	}
	return (pid);
}

/*
** This function launches the checker process, returning its pid. It
** requires the pipe to read from.
*/

static pid_t	launch_checker(char *checker_exe, char **stack_array,
		int pipefds[2])
{
	pid_t	pid;
	char	**argv;

	pid = fork();
	if (pid == -1)
		perror("Can't spawn checker process");
	else if (pid == 0)
	{
		argv = bottom_append_string_array(stack_array, "-v");
		argv = bottom_append_string_array(argv, checker_exe);
		close(pipefds[1]);
		dup2(pipefds[0], 0);
		execv(checker_exe, argv);
		perror("Can't launch push_swap");
		exit(1);
	}
	return (pid);
}

/*
** This function, depending on if args->push_swap_exe is set, will launch
** a push_swap subprocess and set its output to the writing end of the pipe
** returning the pid of the process. If args->push_swap_exe is NULL, it will
** set stdin to the writing end of the pipe and will return -2.
** It will return -1 if the launch of the subprocess failed.
*/

static pid_t	set_instructions_input(struct s_tester_args *args,
		char **stack_array, int pipefds[2])
{
	pid_t	pid_push_swap;

	if (args->push_swap_exe)
	{
		pid_push_swap = launch_push_swap(args->push_swap_exe, stack_array,
				pipefds);
		if (pid_push_swap == -1)
		{
			close(pipefds[1]);
			close(pipefds[0]);
			return (-1);
		}
	}
	else
	{
		pid_push_swap = -2;
		close(pipefds[0]);
		dup2(0, pipefds[1]);
	}
	return (pid_push_swap);
}

/*
** This function launches the push_swap process (if set in args) and the
** checker process, piping them and awaiting for the checker to finish.
** Returns 0 when the chekcer indicates a succesfull test.
*/

int	launch_subprocesses(struct s_tester_args *args, char **stack_array)
{
	int		pipefds[2];
	pid_t	pid_push_swap;
	pid_t	pid_checker;
	int		stat_loc;
	int		checker_exit_error;

	pipe(pipefds);
	pid_push_swap = set_instructions_input(args, stack_array, pipefds);
	if (pid_push_swap == -1)
		return (1);
	pid_checker = launch_checker(args->checker_exe, stack_array, pipefds);
	if (pid_push_swap > 0)
	{
		waitpid(pid_push_swap, &stat_loc, 0);
		close(pipefds[1]);
	}
	checker_exit_error = 1;
	if (pid_checker != -1)
	{
		waitpid(pid_checker, &stat_loc, 0);
		checker_exit_error = !(WIFEXITED(stat_loc) && !WEXITSTATUS(stat_loc));
	}
	close(pipefds[0]);
	return (checker_exit_error);
}
