/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bottom_append_string_array.c                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/01 18:14:36 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/01 20:20:20 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** tester.h: This file implements one of its functions.
** stdlib.h: calloc()
** stdio.h: perror()
*/

#include "tester.h"
#include <stdlib.h>
#include <stdio.h>

/*
** This function allocates a new array containing the append string as it
** first element and then all the elements of the original array.
*/

char	**bottom_append_string_array(char **array, char *append)
{
	char	**new;
	size_t	size_orig;
	size_t	i;

	size_orig = 0;
	while (array[size_orig])
		size_orig++;
	new = calloc(sizeof(char *), size_orig + 2);
	if (!new)
	{
		perror("Can't allocate string array");
		return (NULL);
	}
	new[0] = append;
	i = 1;
	while (array[i - 1])
	{
		new[i] = array[i - 1];
		i++;
	}
	return (new);
}
