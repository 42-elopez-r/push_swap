/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tester_display_stack.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/01 18:48:03 by elopez-r          #+#    #+#             */
/*   Updated: 2021/04/02 19:29:46 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** INCLUDES:
** tester.h: This file implements one of its functions
** stdio.h: puts(), printf(), fflush()
*/

#include "tester.h"
#include <stdio.h>

/*
** This function displays a header with the generated stack.
*/

void	display_stack(char **stack_array)
{
	puts("INITIAL STACK:\n----------------");
	printf("TOP: ");
	while (*stack_array)
		printf("%s ", *stack_array++);
	puts("\n----------------");
	fflush(stdout);
}
